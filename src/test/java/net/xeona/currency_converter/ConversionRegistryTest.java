package net.xeona.currency_converter;

import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ConversionRegistryTest {

    private Map<Currency, ConversionRegistry.Rate> registry;
    private ConversionRegistry subjectUnderTest;

    @Before
    public void beforeTest() {
        registry = ImmutableMap.<Currency, ConversionRegistry.Rate>builder()
                .put(Currency.getInstance("GBP"), new ConversionRegistry.Rate("United Kingdom", "Pounds", new BigDecimal(1.0)))
                .put(Currency.getInstance("USD"), new ConversionRegistry.Rate("United States", "Dollars", new BigDecimal(1.5)))
                .build();
        subjectUnderTest = new ConversionRegistry(registry);
    }

    // TODO: Check the exception messages too

    @Test(expected = IllegalArgumentException.class)
    public void raisesIllegalArgumentExceptionOnUnrecognisedSource() {
        subjectUnderTest.convert(Currency.getInstance("EUR"), new BigDecimal(1.0), Currency.getInstance("GBP"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void raisesIllegalArgumentExceptionOnUnrecognisedTarget() {
        subjectUnderTest.convert(Currency.getInstance("GBP"), new BigDecimal(1.0), Currency.getInstance("EUR"));
    }

    @Test
    public void convertsSuccessfullyBetweenCurrencies() {
        ConversionRegistry.Result result = subjectUnderTest.convert(Currency.getInstance("GBP"), new BigDecimal(3.13), Currency.getInstance("USD"));
        assertThat(result.getCountry(), is("United States"));
        assertThat(result.getCurrencyName(), is("Dollars"));
        assertThat(result.getAmount(), is(new BigDecimal("4.70")));
    }

    @Test(expected = IllegalArgumentException.class)
    public void raisesExceptionIfIllegalLineParsed() throws IOException {
        Reader reader = new StringReader("Hello, World, Here's, My, Conversion, Specification");
        ConversionRegistry.parseRates(reader);
    }

    @Test(expected = IllegalArgumentException.class)
    public void raisesExceptionIfIllegalCurrencyCodeUsed() throws IOException {
        Reader reader = new StringReader("Country, Currency, AAA, 1.0");
        ConversionRegistry.parseRates(reader);
    }

    @Test
    public void parsesRatesFromValidInput() throws IOException {
        Reader reader = new StringReader("United Kingdom, Pounds, GBP, 1.0\n" +
                "United States, Dollars, USD, 1.5");
        Map<Currency, ConversionRegistry.Rate> rates = ConversionRegistry.parseRates(reader);
        assertThat(rates, is(ImmutableMap.builder()
                .put(Currency.getInstance("GBP"), new ConversionRegistry.Rate("United Kingdom", "Pounds", new BigDecimal("1.0")))
                .put(Currency.getInstance("USD"), new ConversionRegistry.Rate("United States", "Dollars", new BigDecimal("1.5")))
                .build()));
    }

}
