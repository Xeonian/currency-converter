package net.xeona.currency_converter;

import java.math.BigDecimal;
import java.util.Currency;

import static com.google.common.base.Preconditions.checkArgument;

public class Main {

    private static final ConversionRegistry CONVERSION_REGISTRY = ConversionRegistry.load();

    public static void main(String... args) {
        try {
            checkArgument(args.length == 3, "Insufficient arguments provided");

            ConversionRegistry.Result targetAmount = doMain(args[0], args[1], args[2]);
            System.out.println(String.format("%s %s %s", targetAmount.getCountry(), targetAmount.getCurrencyName(), targetAmount.getAmount()));
        } catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
            // TODO: Print help text (is there a nice Java library for this? In Python, Typer handles all this cruft for you)
            System.exit(1);
        }
    }

    static ConversionRegistry.Result doMain(String amountStr, String sourceCurrencyCode, String targetCurrencyCode) {
        BigDecimal amount;
        try {
            amount = new BigDecimal(amountStr);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Amount format not recognised", e);
        }
        Currency sourceCurrency = Currency.getInstance(sourceCurrencyCode);
        Currency targetCurrency = Currency.getInstance(targetCurrencyCode);

        return CONVERSION_REGISTRY.convert(sourceCurrency, amount, targetCurrency);
    }

}