package net.xeona.currency_converter;

import com.google.common.collect.ImmutableMap;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public class ConversionRegistry {

    private static final Pattern RATE_LINE_PATTERN = Pattern.compile("([\\w\\s]+), ([\\w\\s]+), (\\w{3}), (\\d+\\.\\d+)");

    private final Map<Currency, Rate> registry;

    ConversionRegistry(Map<Currency, Rate> registry) {
        this.registry = ImmutableMap.copyOf(registry);
    }

    public Result convert(Currency source, BigDecimal amount, Currency target) {
        checkArgument(registry.containsKey(source), String.format("No registered conversion rate for source currency %s", source));
        checkArgument(registry.containsKey(target), String.format("No registered conversion rate for target currency %s", target));

        Rate sourceRate = registry.get(source);
        BigDecimal amountGbp = amount.divide(sourceRate.rate, 2, RoundingMode.UP);

        Rate targetRate = registry.get(target);
        BigDecimal targetAmount = amountGbp.multiply(targetRate.rate);

        return Result.fromRate(targetRate, targetAmount.setScale(2, RoundingMode.UP));
    }

    public static ConversionRegistry load() {
        try {
            InputStream conversionRateStream = ConversionRegistry.class.getResourceAsStream("/conversion_rates.csv");
            Reader conversionRateReader = new InputStreamReader(conversionRateStream);
            return new ConversionRegistry(parseRates(conversionRateReader));
        } catch (IOException e) {
            throw new AssertionError("Failed to parse conversion rates from source file", e);
        }
    }

    static Map<Currency, Rate> parseRates(Reader reader) throws IOException {
        ImmutableMap.Builder<Currency, Rate> registryBuilder = ImmutableMap.builder();
        try (BufferedReader bufferedReader = new BufferedReader(reader)) {
            for (String line = bufferedReader.readLine(); line != null; line = bufferedReader.readLine()) {
                Matcher rateLineMatcher = RATE_LINE_PATTERN.matcher(line);
                if (!rateLineMatcher.matches()) {
                    throw new IllegalArgumentException(
                            String.format("Failed to parse conversion rates from line: \n\t%s", line));
                }
                String countryName = rateLineMatcher.group(1);
                String currencyName = rateLineMatcher.group(2);

                String currencyCode = rateLineMatcher.group(3);
                Currency currency = Currency.getInstance(currencyCode);

                String conversionRateString = rateLineMatcher.group(4);
                BigDecimal conversionRate = new BigDecimal(conversionRateString);

                registryBuilder.put(currency, new Rate(countryName, currencyName, conversionRate));
            }
        }

        return registryBuilder.build();
    }

    public static class Result {

        private final String country;
        private final String currencyName;
        private final BigDecimal amount;

        public Result(String country, String currencyName, BigDecimal amount) {
            this.country = checkNotNull(country);
            this.currencyName = checkNotNull(currencyName);
            this.amount = checkNotNull(amount);
        }

        public String getCountry() {
            return country;
        }

        public String getCurrencyName() {
            return currencyName;
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public static Result fromRate(Rate rate, BigDecimal amount) {
            return new Result(rate.country, rate.currencyName, amount);
        }

    }

    static class Rate {

        private final String country;
        private final String currencyName;
        private final BigDecimal rate;

        public Rate(String country, String currencyName, BigDecimal rate) {
            this.country = checkNotNull(country);
            this.currencyName = checkNotNull(currencyName);
            this.rate = checkNotNull(rate);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Rate rate1 = (Rate) o;
            return country.equals(rate1.country) && currencyName.equals(rate1.currencyName) && rate.equals(rate1.rate);
        }

        @Override
        public int hashCode() {
            return Objects.hash(country, currencyName, rate);
        }
    }

}
