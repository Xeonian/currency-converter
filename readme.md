# Currency Converter

A simple currency converter using hard-coded conversion rates.

## Usage

This converter is a command line tool requiring three arguments:

* The amount to be converted, in the form #.##
* The source currency, as a three-character ISO 4217 currency code
* The target currency, as a three-character ISO 4217 currency code

The app only contains conversion rates for the following currencies:

* GBP
* AED
* AUD
* BAM
* BGN

Attempting to convert to or from any other currencies will result in failure.

## Outstanding tasks

There are still shortcomings in this implementation.

### Boilerplate CLI features

The converter as implemented is not a good citizen as a CLI tool, as it only provides rudimentary error reporting and 
does not provide a comprehensive help message defining the application's purpose and required arguments.

### Documentation

Docstrings have so far been excluded from the project for speed of development. These should be introduced.

### Improved unit test coverage

A portion of functionality has not been unit tested, primarily due to its interaction with static variables, which are 
not readily mocked in Java. The code could be refactored to accommodate this.

### Feature testing

Gherkin should be used to introduce feature testing, to exercise the integrated CLI tool when invoked at the command 
line.